import json, os, re
import argparse
import urllib.request
import urllib.parse
import subprocess
from argparse import RawTextHelpFormatter

SUBSCRIBTION_TEXT = "vs.txt"
directory_path = os.path.dirname(os.path.realpath(__file__))


def remove_chinese_letters(text):
    pattern = re.compile("[\u4e00-\u9fff]+")  # Matches any Chinese character
    return re.sub(pattern, "", text).replace(" ", "")


def add_chain(chain_name):
    with open(os.path.join(directory_path, chain_name + ".JSON"), "r") as jump:
        jump_obj = json.loads(jump.read())

    dns_str = """{
        "servers": [
            "https://dns.nextdns.io/76bcfa/v2",
            "https://1.1.1.1/dns-query",
            "localhost"
        ],

        "queryStrategy": "UseIPv4",
        "disableCache": false,
        "disableFallback": true,
        "tag": "dns_inbound"
        }"""
    dns_obj = json.loads(dns_str)

    for filename in os.listdir(directory_path):
        if not filename.startswith("j") and filename.endswith(".json"):
            with open(filename, "r") as js0:
                original_json = json.loads(js0.read())
                original_json["outbounds"].insert(0, jump_obj)
                original_json["outbounds"][1]["tag"] = "jump"
                # original_json['dns'] = dns_obj
                original_json["routing"]["domainMatcher"] = "mph"

            # filename_without_extension = os.path.splitext(filename)[0]
            with open(chain_name + remove_chinese_letters(filename), "w") as js1:
                js1.write(json.dumps(original_json))


def read_subscribe(sub_url):
    print("Reading from subscribe ...")

    if sub_url.startswith("http"):
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3"
        }
        req = urllib.request.Request(url=sub_url, headers=headers)
        with urllib.request.urlopen(req) as response:
            _subs = response.read()
            with open(SUBSCRIBTION_TEXT, "wb") as f:
                f.write(_subs)
                print("subscribe saved to vs.txt")


def parse_all():
    command = f"python vmess2json.py --parse_all --inbounds socks:44082,http:44083 < {SUBSCRIBTION_TEXT}"
    subprocess.run(command, shell=True)


def clear_files(starts_with):
    command = f"rm -f {starts_with}*"
    subprocess.run(command, shell=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""vmess2json convert vmess link to client json config.
        python v2j.py --save_vmess_subscribtion ''
        python v2j.py --parse_all_subscribtion 1
        python v2j.py --clear jLsj
        python v2j.py --add_chain jOr
        """,
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument("--save_subscribtion", help="save the subscribtion to vs.txt")

    parser.add_argument(
        "--parse_all_subscribtion",
        default="",
        help="parse all the subscribtion in vs.txt",
    )

    parser.add_argument("--add_chain", help="add chain to jsons")

    parser.add_argument("--clear", help="clear files starts with")

    option = parser.parse_args()

    if option.save_subscribtion:
        read_subscribe(option.save_subscribtion)

    if option.parse_all_subscribtion != "":
        parse_all()

    if option.add_chain:
        add_chain(option.add_chain)

    if option.clear:
        clear_files(option.clear)
